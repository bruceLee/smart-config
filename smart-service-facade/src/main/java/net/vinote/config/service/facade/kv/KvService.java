package net.vinote.config.service.facade.kv;

/**
 * 键值对查询服务
 * 
 * @author Seer
 * @version KvService.java, v 0.1 2016年6月6日 下午3:58:31 Seer Exp.
 */
public interface KvService {

	/**
	 * 查询指定租户下的键值信息
	 * @param tenant 租户Code
	 * @param key 键名
	 * @return
	 */
	public String queryValue(String tenant, String key);
}
