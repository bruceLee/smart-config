package net.vinote.config.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication(scanBasePackages={"net.vinote.config"})
@ImportResource(
	locations = { "classpath*:service-config.xml","classpath*:mvc-config.xml" })
public class BootStrap {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BootStrap.class, args);
	}
}
