package net.vinote.config.service.api.handler.tenant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import net.vinote.config.integration.cache.CacheClient;
import net.vinote.config.service.api.ApiAuthBean;
import net.vinote.config.service.api.RestApiHandler;
import net.vinote.config.service.facade.result.Pagination;
import net.vinote.config.service.util.AssertUtils;
import net.vinote.config.shared.SmartException;

/**
 * 租户Handler
 * 
 * @author Seer
 * @version TenantHander.java, v 0.1 2016年6月1日 下午2:41:43 Seer Exp.
 */
public class TenantHander implements RestApiHandler {
	/** 租户标识 */
	private static final String TENANT_FLAG = "tenant";
	@Autowired
	private CacheClient cacheClient;

	@Override
	public Object execute(ApiAuthBean authBean, Map<String, String> params) {
		String act = authBean.getActName();
		if (StringUtils.equals("list", act)) {
			return queryTenantList(authBean, params);
		} else if (StringUtils.equals("get", act)) {
			return getTenant(authBean, params);
		} else if (StringUtils.equals("add", act)) {
			return addTenant(authBean, params);
		} else if (StringUtils.equals("del", act)) {
			return delTenant(authBean, params);
		} else if (StringUtils.equals("update", act)) {
			return updateTenant(authBean, params);
		}

		throw new SmartException("unsupport operation");
	}

	private Object updateTenant(ApiAuthBean authBean, Map<String, String> params) {
		String code = params.get("code");
		AssertUtils.isNotBlank(code, "租户Code不能为空");
		String name = params.get("name");
		AssertUtils.isNotBlank(name, "租户名称不能为空");
		String intr = StringUtils.defaultIfBlank(params.get("intr"), "");
		JSONObject json = cacheClient.getObject(TENANT_FLAG + code);
		AssertUtils.isTrue(json != null, "该租户不存在");
		json = new JSONObject();
		json.put("code", code);
		json.put("name", name);
		json.put("intr", intr);
		cacheClient.putObject(TENANT_FLAG + code, json, -1);
		return null;
	}

	private Object delTenant(ApiAuthBean authBean, Map<String, String> params) {
		String code = params.get("code");
		AssertUtils.isNotBlank(code, "租户Code不能为空");
		long effect = cacheClient.remove(TENANT_FLAG + code);
		AssertUtils.isTrue(effect > 0, "删除失败");
		return null;
	}

	private Object getTenant(ApiAuthBean authBean, Map<String, String> params) {
		String code = StringUtils.defaultIfBlank(params.get("code"), "");
		AssertUtils.isNotBlank(code, "请指定租户Code");
		JSONObject json = cacheClient.getObject(TENANT_FLAG + code);
		AssertUtils.isNotNull(json, "租户信息不存在");
		return json;
	}

	private Object addTenant(ApiAuthBean authBean, Map<String, String> params) {
		String code = params.get("code");
		AssertUtils.isNotBlank(code, "租户Code不能为空");
		String name = params.get("name");
		AssertUtils.isNotBlank(name, "租户名称不能为空");
		String intr = params.get("intr");
		JSONObject json = cacheClient.getObject(TENANT_FLAG + code);
		AssertUtils.isTrue(json == null, "该租户已存在");
		json = new JSONObject();
		json.put("code", code);
		json.put("name", name);
		json.put("intr", intr);
		cacheClient.putObject(TENANT_FLAG + code, json, -1);
		return null;
	}

	private Object queryTenantList(ApiAuthBean authBean, Map<String, String> params) {
		String tenant = StringUtils.defaultIfBlank(params.get("tenant"), "");
		List<String> keyList = cacheClient.keys(TENANT_FLAG + tenant + "*");
		AssertUtils.notEmpty(keyList, "租户信息不存在");
		Pagination<JSONObject> tenants = new Pagination<>();
		tenants.setPage(1);
		tenants.setTotal(keyList.size());
		tenants.setPageSize(keyList.size());
		List<JSONObject> list = new ArrayList<>(keyList.size());
		for (String key : keyList) {
			list.add(cacheClient.getObject(key));
		}
		tenants.setData(list);
		return tenants;
	}

	@Override
	public boolean needTransaction(ApiAuthBean authBean, Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

}
