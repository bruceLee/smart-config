package net.vinote.config.service.api.handler.kv;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import net.vinote.config.integration.cache.CacheClient;
import net.vinote.config.service.api.ApiAuthBean;
import net.vinote.config.service.api.RestApiHandler;
import net.vinote.config.service.facade.result.Pagination;
import net.vinote.config.service.util.AssertUtils;
import net.vinote.config.service.util.ConfigConstant;
import net.vinote.config.shared.SmartException;

/**
 * 键值对Handler
 * 
 * @author Seer
 * @version KeyValueHander.java, v 0.1 2016年5月31日 下午7:54:51 Seer Exp.
 */
public class KeyValueHander implements RestApiHandler {
	@Autowired
	private CacheClient cacheClient;

	@Override
	public Object execute(ApiAuthBean authBean, Map<String, String> params) {
		String act = authBean.getActName();
		if (StringUtils.equals("list", act)) {
			return queryByKey(authBean, params);
		} else if (StringUtils.equals("get", act)) {
			return getKV(authBean, params);
		} else if (StringUtils.equals("add", act)) {
			return addValue(authBean, params);
		} else if (StringUtils.equals("update", act)) {
			return updateValue(authBean, params);
		} else if (StringUtils.equals("del", act)) {
			return del(authBean, params);
		}
		throw new SmartException("unsupport operation");
	}

	private Object getKV(ApiAuthBean authBean, Map<String, String> params) {
		String tenant = StringUtils.defaultIfBlank(params.get("tenant"), "");
		AssertUtils.isNotBlank(tenant, "请先指定租户");
		String key = params.get("key");
		AssertUtils.isNotBlank(key, "key不能为空");
		JSONObject json = cacheClient.getObject(tenant + "." + ConfigConstant.KV + key);
		AssertUtils.isNotNull(json, "键值对信息不存在");
		return json;
	}

	/**
	 * 删除键值
	 * 
	 * @param authBean
	 * @param params
	 * @return
	 */
	private Object del(ApiAuthBean authBean, Map<String, String> params) {
		String tenant = StringUtils.defaultIfBlank(params.get("tenant"), "");
		AssertUtils.isNotBlank(tenant, "请先指定租户");
		String key = params.get("key");
		AssertUtils.isNotBlank(key, "key不能为空");
		long effect = cacheClient.remove(tenant + "." + ConfigConstant.KV + key);
		AssertUtils.isTrue(effect > 0, "删除失败");
		return null;
	}

	private Object updateValue(ApiAuthBean authBean, Map<String, String> params) {
		String tenant = StringUtils.defaultIfBlank(params.get("tenant"), "");
		AssertUtils.isNotBlank(tenant, "请先指定租户");
		String key = params.get("key");
		AssertUtils.isNotBlank(key, "key不能为空");
		String value = params.get("value");
		AssertUtils.isNotBlank(value, "value不能为空");
		String remark = StringUtils.defaultIfBlank(params.get("remark"), "");
		boolean exists = cacheClient.exists(tenant + "." + ConfigConstant.KV + key);
		AssertUtils.isTrue(exists, "该键名不存在");

		JSONObject json = new JSONObject();
		json.put("key", key);
		json.put("value", value);
		json.put("remark", remark);
		cacheClient.putObject(tenant + "." + ConfigConstant.KV + key, json, -1);
		return 1;
	}

	/**
	 * 新增键值对
	 * 
	 * @param authBean
	 * @param params
	 * @return
	 */
	private Object addValue(ApiAuthBean authBean, Map<String, String> params) {
		String tenant = StringUtils.defaultIfBlank(params.get("tenant"), "");
		AssertUtils.isNotBlank(tenant, "请先指定租户");
		String key = params.get("key");
		AssertUtils.isNotBlank(key, "key不能为空");
		String value = params.get("value");
		AssertUtils.isNotBlank(value, "value不能为空");
		String remark = StringUtils.defaultIfBlank(params.get("remark"), "");
		boolean exists = cacheClient.exists(tenant + "." + ConfigConstant.KV + key);
		AssertUtils.isFalse(exists, "该键名已存在");

		JSONObject json = new JSONObject();
		json.put("key", key);
		json.put("value", value);
		json.put("remark", remark);
		cacheClient.putObject(tenant + "." + ConfigConstant.KV + key, json, -1);
		return 1;
	}

	private Object queryByKey(ApiAuthBean authBean, Map<String, String> params) {
		String tenant = StringUtils.defaultIfBlank(params.get("tenant"), "");
		AssertUtils.isNotBlank(tenant, "请先选择租户");
		String key = StringUtils.defaultIfBlank(params.get("key"), "");
		List<String> keyList = cacheClient.keys(tenant + "." + ConfigConstant.KV + key + "*");
		AssertUtils.notEmpty(keyList, "查询信息不存在");
		Pagination<JSONObject> keyPagination = new Pagination<>();
		keyPagination.setPage(1);
		keyPagination.setTotal(keyList.size());
		keyPagination.setPageSize(keyList.size());
		List<JSONObject> list = new ArrayList<>(keyList.size());
		for (String k : keyList) {
			list.add(cacheClient.getObject(k));
		}
		keyPagination.setData(list);
		return keyPagination;
	}

	@Override
	public boolean needTransaction(ApiAuthBean authBean, Map<String, String> params) {
		// TODO Auto-generated method stub
		return false;
	}

}
