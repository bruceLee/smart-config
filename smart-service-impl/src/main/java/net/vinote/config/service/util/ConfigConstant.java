package net.vinote.config.service.util;

/**
 * 配置中心常量
 * 
 * @author Seer
 * @version ConfigConstant.java, v 0.1 2016年6月6日 下午4:07:46 Seer Exp.
 */
public interface ConfigConstant {
	/** 键值对标识 */
	public static final String KV = "kv.";
}
