package net.vinote.config.service.kv.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import net.vinote.config.integration.cache.CacheClient;
import net.vinote.config.service.facade.kv.KvService;
import net.vinote.config.service.util.AssertUtils;
import net.vinote.config.service.util.ConfigConstant;

/**
 * 键值对服务实现
 * 
 * @author Seer
 * @version KvServiceImpl.java, v 0.1 2016年6月6日 下午4:09:31 Seer Exp.
 */
public class KvServiceImpl implements KvService {
	
	@Autowired
	private CacheClient cacheClient;

	@Override
	public String queryValue(String tenant, String key) {
		AssertUtils.isNotBlank(tenant, "请先指定租户");
		AssertUtils.isNotBlank(key, "key不能为空");
		JSONObject json = cacheClient.getObject(tenant + "." + ConfigConstant.KV + key);
		AssertUtils.isNotNull(json, "键值对信息不存在");
		return json.getString("value");
	}

}
